package co.edu.uco.objetorelacion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ObjetoRelacionApplication {

    public static void main(String[] args) {
        SpringApplication.run(ObjetoRelacionApplication.class, args);
    }

}

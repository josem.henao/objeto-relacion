package co.edu.uco.objetorelacion.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;
import java.util.List;


@Table(name = "usuario")
@Entity
@AllArgsConstructor
@Data
public class Usuario {
    @Id
    @Column(name = "id")
    @GeneratedValue
    private Long id;

    @Column(name= "nombre")
    private String nombre;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "usuario_rol", joinColumns = @JoinColumn(name = "id_usuario"), inverseJoinColumns = @JoinColumn(name = "id_rol"))
    private List<Rol> roles;

}

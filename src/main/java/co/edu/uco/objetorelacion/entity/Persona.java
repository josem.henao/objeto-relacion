package co.edu.uco.objetorelacion.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;

@Data
@AllArgsConstructor
@Entity
@Table(name = "persona")
public class Persona {
    @Id
    @Column(name = "id")
    @GeneratedValue//(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "nombre")
    private String nombre;

    @ManyToOne(cascade = {CascadeType.PERSIST})
    @JoinColumn(name = "id_usuario")
    private Usuario usuario;


}
